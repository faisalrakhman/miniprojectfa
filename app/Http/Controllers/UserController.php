<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\support\facades\session;

class UserController extends Controller
{
	public function list () {
		$fa_absen = DB::table('peserta')->get();
		//dd($fa_absen);
		return view ('user.list',compact('fa_absen'));
	}
	public function input($id) {
		$data = DB::table('peserta')->where('id', $id)->first();
		return view ('user.form',compact('data'));
		
	}
	public function save(request $req, $id) {
		
		$messages = [
			'nama.required' => 'Harus Diisi, jangan kosong',
			'nik.required' => 'nik tidak boleh kosong',
			'max' => ':attribute harus diisi maksimal :max
			karakter !!!',
		];
		$this->validate($req,[
			'nama' => 'required|min:5|max:20',
			'nik' => 'required|numeric',
			'unit' => 'required',
			'email' => 'required',
		], $messages);
		$exists=DB::table('peserta')->where('id',$id)->first();
		if($exists){
			DB::table('peserta')->where('id', $id)->update([
			"nama"		=> $req->nama,
			"nik"		=> $req->nik,
			"unit"		=> $req->unit,
			"email"		=> $req->email,
		]);
	}else{
		
		DB::table('peserta')->insert([
			"nama"		=> $req->nama,
			"nik"		=> $req->nik,
			"unit"		=> $req->unit,
			"email"		=> $req->email
		]);
	}
		return redirect('/home');
		
	}
	public function delete($id){
		DB::table('peserta')->where('id',$id)->delete();
		return redirect('/home');
	}
/*	public function login(request $req){
		$exists = DB::table('user')	
		->where('username', $req->username)
		->where('password', $req->password)->first();
	
	if($exists){
		Session::put('auth',$exists);
		return redirect('/');
	}else{
		return redirect('/login');
		}
	}
	public function logout(){
		Session::forget('auth');
		return redirect('/login');
	}
*/	
}
