@extends('layout')
@section('contents')
@if(count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
<form method="post">
  
  <div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0">
      <input type="text" class="form-control form-control-user" id="nama" placeholder="Isi Nama" name="nama"
      value="{{ isset($data->nama)? $data->nama : '' }}">
    </div>
    <div class="col-sm-6">
      <input type="text" class="form-control form-control-user" id="nik" placeholder="Isi NIK" name="nik"
      value="{{ isset($data->nik)? $data->nik : '' }}">
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0">
      <input type="text" class="form-control form-control-user" id="unit" placeholder="Unit"name="unit"
      value="{{ isset($data->unit)? $data->unit : '' }}">
    </div>
    <div class="col-sm-6">
      <input type="text" class="form-control form-control-user" id="email" placeholder="Isi email" name="email"
      value="{{ isset($data->email)? $data->email : '' }}">
    </div>
  </div>
  <button type="submit" class="btn btn-success btn-block"><span class="fas fa-save 
   fa-sm text-white-50"></span> Save</button>
 </form>
 @endsection