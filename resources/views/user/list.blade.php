@extends('layout')
@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Daftar Peserta</h1>
            <a href="/home/input" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-plus fa-sm text-white-50"></i> Tambah </a>
          </div>
	<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                 <thead>
                    <tr>
                    <th>#</th>
					<th>Nama</th>
					<th>NIK</th>
					<th>Unit</th>
					<th>Email</th>
					<th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
			@foreach($fa_absen as $no => $data)
			<tr>
				<th>{{ ++$no }}</th>
				<th>{{ $data->nama }}</th>
				<th>{{ $data->nik }}</th>
				<th>{{ $data->unit }}</th>
				<th>{{ $data->email }}</th>
				<th><a href="/home/{{ $data->id}}" class="btn-info btn-sm"><i class="fas fa-edit fa-sm text-white-50">
				</i> Edit </a>
				<a href="/deleteuser/{{ $data->id }}" class="btn-danger btn-sm "><i class="fas fa-trash fa-sm text-white-50">
				</i> Delete</a></th>
			</tr>
			@endforeach
  </tbody>
</table>
</div>
@endsection