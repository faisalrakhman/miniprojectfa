<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/home','UserController@list');
Route::get('/home/{id}','UserController@input');
Route::post('/home/{id}','UserController@save');
Route::get('/deleteuser/{id}','UserController@delete');
